package plug.language.random;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import plug.core.IRuntimeView;
import plug.core.view.ConfigurationItem;
import plug.language.random.runtime.Config;
import plug.language.random.runtime.RandomRuntime;

/**
 * Created by Ciprian TEODOROV on 04/03/17.
 */
public class RandomRuntimeView implements IRuntimeView<Config, Integer> {
    final private RandomRuntime runtime;

    public RandomRuntimeView(RandomRuntime runtime) {
        this.runtime = runtime;
    }
    @Override
    public RandomRuntime getRuntime() {
        return runtime;
    }

	@Override
	public List<ConfigurationItem> getConfigurationItems(Config value) {
		return Collections.singletonList(new ConfigurationItem("random", Arrays.toString(value.getArr()), null, null));
	}

	@Override
	public String getFireableTransitionDescription(Integer transition) {
		return Integer.toString(transition);
	}

}
