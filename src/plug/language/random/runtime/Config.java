package plug.language.random.runtime;

import plug.core.IConfiguration;
import plug.core.defaults.DefaultConfiguration;
import plug.explorer.buchi.DefaultExecutionController;

import java.util.Arrays;

/**
 * Created by Ciprian TEODOROV on 03/03/17.
 */
public class Config extends DefaultConfiguration<Config> {
    Boolean[] arr;

    public Boolean[] getArr() {
        return arr;
    }

    Config(int size) {
        arr = new Boolean[size];
        Arrays.fill(arr, Boolean.FALSE);
    }

    @Override
    public Config createCopy() {
        Config newC = new Config(arr.length);
        newC.arr = arr.clone();
        return newC;
    }

    @Override
    public String toString() {
        return Arrays.toString(arr);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        return Arrays.equals(arr,((Config)obj).arr);
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(arr);
    }
}
