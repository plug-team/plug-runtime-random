package plug.language.random.runtime;

import plug.core.*;
import plug.statespace.transitions.FiredTransition;

import java.util.*;

public class RandomRuntime implements ITransitionRelation<Config, Integer> {
	Random random = new Random();
	int maxTransitions = 100;
	int changeSize = 2;
	int configurationSize = 2;
	
	public RandomRuntime(int configurationSize, int changeSize, int maxTransitions) {
		this.configurationSize = configurationSize;
		this.changeSize = configurationSize < changeSize ? configurationSize : changeSize;
		this.maxTransitions = maxTransitions;
	}
	
	public RandomRuntime(int configurationSize) {
		this.configurationSize = configurationSize;
		this.changeSize = configurationSize;
	}

	public Set<Config> initialConfigurations() {		
		return Collections.singleton(new Config(configurationSize));
	}

	@Override
	public Set<Integer> fireableTransitionsFrom(Config source) {
		int nbTransitions = random.nextInt(maxTransitions)+1;

		Set<Integer> fireableSet = new HashSet<>(nbTransitions);
		for (int i = 0; i < nbTransitions; i++) {
			//define the transition behavior when computing the fireable set.
			int changeIdx = random.nextInt(changeSize);

			fireableSet.add(changeIdx);
		}

		return fireableSet;
	}

	@Override
	public IFiredTransition<Config, Integer> fireOneTransition(Config sourceState, Integer changeIndex) {
		//Create the target memory zone
		Config target = sourceState.createCopy();

		//execute the predefined transition in this new memory zone
		target.arr[changeIndex] = !target.arr[changeIndex];

		//give back the results "the fired transition instance"
		return new FiredTransition<>(sourceState, target, changeIndex);
	}
}

