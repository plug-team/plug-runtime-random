package plug.language.random;

import plug.core.ILanguageLoader;
import plug.core.ILanguagePlugin;
import plug.core.IRuntimeView;
import plug.language.random.runtime.RandomRuntime;

/**
 * Created by Ciprian TEODOROV on 03/03/17.
 */
public class RandomPlugin implements ILanguagePlugin<RandomRuntime> {
    @Override
    public String[] getExtensions() {
        return new String[0];
    }

    @Override
    public String getName() {
        return "Random";
    }

    @Override
    public ILanguageLoader getLoader() {
        return (modelURI, options) -> new RandomRuntime(20);
    }

    @Override
    public IRuntimeView getRuntimeView(RandomRuntime runtime) {
        return new RandomRuntimeView(runtime);
    }
}
