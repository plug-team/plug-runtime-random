package plug.language.random;

import announce4j.Announcer;
import org.junit.Test;
import plug.composite.CompositeConfiguration;
import plug.composite.CompositeRuntime;
import plug.core.ILanguagePlugin;
import plug.core.ITransitionRelation;
import plug.events.ExecutionEndedEvent;
import plug.explorer.BFSExplorer;
import plug.language.random.runtime.Config;
import plug.language.random.runtime.RandomRuntime;
import plug.statespace.SimpleStateSpaceManager;
import plug.verifiers.observers.ObserverVerifier;
import plug.verifiers.observers.RejectReachedEvent;
import plug.verifiers.observers.SimpleObserverRuntime;
import plug.verifiers.predicates.AlwaysFalsePredicate;
import plug.verifiers.predicates.PredicateVerifier;
import plug.verifiers.predicates.PredicateViolationEvent;
import plug.verifiers.predicates.PredicatesVerifiedEvent;
import plug.verifiers.predicates.PredicatesViolatedEvent;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class RandomRuntimeTest {
	ILanguagePlugin module = new RandomPlugin();

	ITransitionRelation load(String fileName) throws Exception {
		return module.getLoader().getRuntime(fileName);
	}

	@Test
	public void testOnlyExploration() {
		BFSExplorer<Config, Object> controller = new BFSExplorer<Config, Object>(new RandomRuntime(2), new SimpleStateSpaceManager());
		controller.getAnnouncer().when(ExecutionEndedEvent.class, (Announcer announcer, ExecutionEndedEvent o) -> {
			System.out.println("Final size: " + o.getSource().getStateSpaceManager().size() + "\n");
			assertEquals(4, o.getSource().getStateSpaceManager().size());
		});

		controller.execute();
	}


	public void testOnlyExploration1() {
		for (int i = 4; i < 18; i++) {
			RandomRuntime runtime = new RandomRuntime(100000, i, 100);
			BFSExplorer<Config, Object> controller = new BFSExplorer<Config, Object>(runtime, new SimpleStateSpaceManager());
			controller.getAnnouncer().when(ExecutionEndedEvent.class, (Announcer announcer, ExecutionEndedEvent o) -> {
				System.out.print(o.getSource().getStateSpaceManager().size() + ";");
				//assertEquals(4, o.getSource().getStateSpaceManager().size());
			});

			System.gc();
			long start = System.currentTimeMillis();

			controller.execute();

			System.gc();
			long delay = System.currentTimeMillis() - start;
			System.out.println( i + ";" + (delay) + ";" + (Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory()) + ";");
		}
	}
	

	@Test
	public void testOnlyExplorationTwoRuntimes() {
		CompositeRuntime composite = new CompositeRuntime();
		composite.addRuntime("random1", new RandomRuntime(2, 2, 100));
		composite.addRuntime("random2", new RandomRuntime(2, 2, 100));
		BFSExplorer controller = new BFSExplorer(composite, new SimpleStateSpaceManager());

		controller.getAnnouncer().when(ExecutionEndedEvent.class, (Announcer announcer, ExecutionEndedEvent o) -> {
			System.out.println("Final size: " + o.getSource().getStateSpaceManager().size() + "\n");
			assertEquals(16, o.getSource().getStateSpaceManager().size());
		});


		controller.execute();
	}

	@Test
	public void testPredicate() {
		RandomRuntime runtime = new RandomRuntime(2, 2, 100);
		BFSExplorer<Config, Object> controller = new BFSExplorer<Config, Object>(runtime, new SimpleStateSpaceManager());

		controller.getAnnouncer().when(ExecutionEndedEvent.class, (Announcer announcer, ExecutionEndedEvent o) -> {
			System.out.println("Final size: " + o.getSource().getStateSpaceManager().size() + "\n");
			assertEquals(4, o.getSource().getStateSpaceManager().size());
		});

		PredicateVerifier<Config> pV = new PredicateVerifier<>(controller.getAnnouncer());

		pV.predicates.add(new AlwaysFalsePredicate<>());
		pV.predicates.add(c -> false);

		pV.announcer.when(PredicateViolationEvent.class, (announcer, event) -> {
			System.err.println("predicate violated: " + event.getViolated());});
		pV.announcer.when(PredicatesVerifiedEvent.class, (announcer, event) -> {
			System.err.println("predicates verified ! ");});
		pV.announcer.when(PredicatesViolatedEvent.class, (announcer, event) -> {
			System.err.println("at least one predicate violated ! ");});

		controller.execute();
	}


	@Test
	public void testPredicateStopOnViolation() {
		BFSExplorer controller = new BFSExplorer(new RandomRuntime(2), new SimpleStateSpaceManager());

		PredicateVerifier<Config> pV = new PredicateVerifier<>(controller.getAnnouncer());

		pV.predicates.add(new AlwaysFalsePredicate<>());
		pV.predicates.add(c -> false);

		pV.announcer.when(PredicateViolationEvent.class, (announcer, event) -> {
			System.err.println("predicate violated: " + event.getViolated());
			controller.getMonitor().hasToFinish();
		});

		controller.getAnnouncer().when(ExecutionEndedEvent.class, (Announcer announcer, ExecutionEndedEvent o) -> {
			System.out.println("Final size: " + o.getSource().getStateSpaceManager().size() + "\n");
			assertEquals(1, o.getSource().getStateSpaceManager().size());
		});

		controller.execute();
	}

	@Test
	public void testObserver() {
		CompositeRuntime compositeRuntime = new CompositeRuntime();
		compositeRuntime.addRuntime("random", new RandomRuntime(2, 2, 100));
		BFSExplorer controller = new BFSExplorer(compositeRuntime, new SimpleStateSpaceManager());

		SimpleObserverRuntime sor = new SimpleObserverRuntime();
		sor.observer.put((Short state, CompositeConfiguration config) -> { Boolean arr[] = ((Config)config.get(0)).getArr(); return arr[1]; }, (short) -1);

		ObserverVerifier oV = new ObserverVerifier(controller.getAnnouncer(), sor);

		boolean hasReject[] = new boolean[1];
		oV.announcer.when(RejectReachedEvent.class, (ann, evt) -> hasReject[0] = true);

		controller.getAnnouncer().when(ExecutionEndedEvent.class, (Announcer announcer, ExecutionEndedEvent o) -> {
			System.out.println("Final size: " + o.getSource().getStateSpaceManager().size() + "\n");
		});

		controller.execute();

		assertTrue(hasReject[0]);
	}

	@Test(expected = RuntimeException.class)
	public void testObserverCastFail() {
		BFSExplorer controller = new BFSExplorer(new RandomRuntime(2), new SimpleStateSpaceManager());

		SimpleObserverRuntime sor = new SimpleObserverRuntime();
		sor.observer.put((Short state, CompositeConfiguration config) -> { Boolean arr[] = ((Config)config.get(0)).getArr(); return arr[1]; }, (short) -1);
		ObserverVerifier oV = new ObserverVerifier(controller.getAnnouncer(), sor);

		boolean hasReject[] = new boolean[1];
		oV.announcer.when(RejectReachedEvent.class, (ann, evt) -> {
			hasReject[0] = true;
		});

		controller.getAnnouncer().when(ExecutionEndedEvent.class, (Announcer announcer, ExecutionEndedEvent o) -> {
			System.out.println("Final size: " + o.getSource().getStateSpaceManager().size() + "\n");
			assertEquals(6, o.getSource().getStateSpaceManager().size());
		});

		controller.execute();

		assertTrue(hasReject[0]);
	}
}

