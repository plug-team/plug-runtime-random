##### Purpose
The Random runtime exist only for development, debug and benchmarking purposes.

The programs are automatically generated at random by the Runtime.

RandomRuntime(int configurationSize, int changeSize, int maxTransitions);


The ``configurationSize`` argument define the size of a boolean array 
that represents the configuration - this can be bigger than ``changeSize`` 
for bigger configurations.
The ``changeSize`` argument gives specifies the size of the part that 
can change in a configuration. 2^changeSize -- represents the statespace size.
The ``maxTransitions`` argument represents the maximum branching during the exploration.
